Gem::Specification.new do |s|
	s.name = 'corpus'
	s.version = '0.0.8'
	s.date = '2012-08-06'
	s.summary = 'Tool to download collections of open source software'
	s.description = 'Corpus is a set of Rake tasks which downloads open source software projects and counts LOC.'
	s.authors = ['Dong Qiu, You Zhou, Martin Velez, Earl Barr, Zhendong Su']
	s.email = 'mvelez999@gmail.com'
	s.files = Dir["{lib,bin,conf,ext}/**/*"] + ["README.rdoc","LICENSE"]
	s.homepage = 'http://bitbucket.org/martinvelez/corpus'
	s.require_paths = ["bin","lib"]
	s.add_dependency('rake','>=0.9.0')
	s.executables << 'corpus'
end
